+function ($) {
  'use strict';
  $("#carousel").carousel({
    swipe: 30
  });
}(jQuery);
